//
//  ViewController.m
//  delegatewithoutsegue
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)ShowSecondButton:(id)sender {
    
    SecondViewController *svc=[[SecondViewController alloc]initWithNibName:@"SecondViewController" bundle:nil ];
    UINavigationController *navController = self.navigationController;
    [navController pushViewController:svc animated:YES];
}
@end
