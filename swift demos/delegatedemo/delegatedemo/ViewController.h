//
//  ViewController.h
//  delegatedemo
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondViewController.h"
@interface ViewController : UIViewController <secondvcDelegate>
@property (weak, nonatomic) IBOutlet UILabel *departmentLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedElementLabel;


@end

