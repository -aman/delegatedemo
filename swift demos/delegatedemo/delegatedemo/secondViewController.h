//
//  secondViewController.h
//  delegatedemo
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import <UIKit/UIKit.h>
@class secondViewController;
@protocol secondvcDelegate <NSObject>
@required
-(void)didselectwith:(secondViewController *)controller department:(NSString *)department;
@end
@interface secondViewController : UITableViewController

@property (nonatomic,weak) id <secondvcDelegate> delegate;
@end
