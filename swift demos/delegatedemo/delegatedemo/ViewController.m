//
//  ViewController.m
//  delegatedemo
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"departments"]) {
        secondViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
}
#pragma mark - secondvcdelegate delegate
-(void)didselectwith : (secondViewController *)controller department:(NSString *)department
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    self.departmentLabel.text=department;
    self.selectedElementLabel.text=@"Selected Element : ";
    [self.navigationController popViewControllerAnimated:NO];
}
@end
