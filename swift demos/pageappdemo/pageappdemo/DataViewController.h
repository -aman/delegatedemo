//
//  DataViewController.h
//  pageappdemo
//
//  Created by daffomac on 2/25/16.
//  Copyright © 2016 demos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@end

